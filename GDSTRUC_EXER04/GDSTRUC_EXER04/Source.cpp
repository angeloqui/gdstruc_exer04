#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "UnorderedArray.h"

using namespace std;

int main() {
	
	// Initialize random seed:
	srand(time(NULL));

	//Create an UnorderedArray of integers based on the
	//size that the user wants.
	int size;
	cout << "Input Array Size: ";
	cin >> size;
	UnorderedArray<int> integers(size);

	//Fill the UnorderedArray with random values from 1 to 100.
	for (int i = 0; i < size; i++) {
		integers.push(rand() % 100 + 1);
	}

	system("cls");

	while (true) {

		cout << "Unordered Array: " << endl;

		//Output everything.
		for (int i = 0; i < integers.getSize(); i++) {
			cout << integers[i] << " ";
		}

		//Options
		int options;
		cout << string(2, '\n') << string(20, '=');
		cout << "\n1. Remove"
			 << "\n2. Search"
			 << "\n\nOptions: ";
		cin >> options;
		
		int n;
		cout << "Number: ";
		cin >> n;

		if (options == 1) {
			integers.remove(n);
		}
		else if (options == 2) {
			cout << "Search result in Array[";
			if (integers.search(n) >= 0) cout << integers.search(n) << "]";
			else cout << "Value not found";
			cout << endl;
		}

		system("pause");
		system("cls");
	}
	return 0;
}